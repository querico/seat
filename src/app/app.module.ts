import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
//importar config firebase
import { firebaseConfig } from '../config/firebase.config';
//importar página login
import { LoginPage } from '../pages/login/login';
//Firebase
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
//importacion del provider usuario
import { UsuarioProvider } from '../providers/usuario/usuario';
import { AjustesPage } from '../pages/ajustes/ajustes';
//plugins
import { Facebook } from '@ionic-native/facebook';
//google plus
import { GooglePlus } from '@ionic-native/google-plus';
//storage
import { IonicStorageModule } from '@ionic/storage';
import { IntroPage } from '../pages/intro/intro';
import { AlmacenamientoService } from '../providers/almacenamiento/almacenamiento';
//onesignal
import { OneSignal } from '@ionic-native/onesignal';
import { PushnotificationProvider } from '../providers/pushnotification/pushnotification';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    AjustesPage,
    IntroPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    IonicStorageModule.forRoot()

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    AjustesPage,
    IntroPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UsuarioProvider,
    Facebook,
    GooglePlus,
    AlmacenamientoService,
    OneSignal,
    PushnotificationProvider
    
  ]
})
export class AppModule {}
