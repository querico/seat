import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { IntroPage } from '../pages/intro/intro';
//importar servicio
import { AlmacenamientoService } from '../providers/almacenamiento/almacenamiento';
import { PushnotificationProvider } from '../providers/pushnotification/pushnotification';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  //rootPage:any = LoginPage;
  rootPage:any;

  constructor(private platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
              private _almacen: AlmacenamientoService,
              public _pushProvider: PushnotificationProvider ) {
    platform.ready().then(() => {
      
      this._almacen.cargar_storage()
      .then( ()=>{

        if( this._almacen.almacen.mostrar_tutorial ){
          this.rootPage = IntroPage;

        }else{
          this.rootPage = LoginPage;
        }

        this.platform.pause.subscribe( ()=>{
          console.log("La aplicación se detendrá");
        });

        this.platform.resume.subscribe(()=>{
          // se puden llamar servicios
          console.log("la aplicacion va a continuar");
        })

        statusBar.styleDefault();
        splashScreen.hide();

        this._pushProvider.init_notifications();

  })
});
  }
}

