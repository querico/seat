import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Credenciales, UsuarioProvider } from '../../providers/usuario/usuario';


@IonicPage()
@Component({
  selector: 'page-ajustes',
  templateUrl: 'ajustes.html',
})
export class AjustesPage {

  user: Credenciales = {};

  constructor(public navCtrl: NavController, public usuarioProv: UsuarioProvider) {
    this.user = this.usuarioProv.usuario;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AjustesPage');
  }

}
