import { Component } from '@angular/core';
import { NavController, MenuController, AlertController } from 'ionic-angular';
import { UsuarioProvider, Credenciales } from '../../providers/usuario/usuario';
import { AjustesPage } from '../ajustes/ajustes';
import { LoginPage } from '../login/login';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  user: Credenciales = {};

  constructor(public navCtrl: NavController, 
    public usuarioProvider: UsuarioProvider,
    private menuCtrl: MenuController,
    private afAuth:AngularFireAuth,
    private alertCtrl: AlertController,
    ) {
    
    console.log(this.usuarioProvider.usuario);
    this.user = this.usuarioProvider.usuario;

    this.afAuth.authState.subscribe( user => {

      console.log('AFAUTH!!!');
      console.log( JSON.stringify(user) );

    });
  }
  
  mostrarMenu(){
    this.menuCtrl.toggle();
  }
  openAjustes(){
    this.navCtrl.push(AjustesPage);
    this.menuCtrl.close();
  }
 
  salir() {

    this.afAuth.auth.signOut().then( res => {

      this.usuarioProvider.usuario = {};
      this.navCtrl.setRoot(LoginPage);

    });
    
  }

  //pregunta si al salir está seguro o no
  ionViewCanLeave(){
    console.log("ionViewWillUnLoad");

    let promesa = new Promise ((resolve, reject)=>{
      let confirmar = this.alertCtrl.create({
        title:"¿Seguro?",
        subTitle: "Salir de la aplicación",
        buttons: [
          {
            text: 'Cancelar',
            handler: () => resolve(false)
          },
          {
            text: 'Salir!',
            handler: () => resolve(true)
          }
        ]
      });
      confirmar.present();
    });
    return promesa;
  }
}
