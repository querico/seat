import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Platform } from 'ionic-angular';


@Injectable()
export class AlmacenamientoService {

  almacen = {
    mostrar_tutorial: true
  }

  constructor(private platform:Platform, private storage: Storage) {
    console.log('Hello AlmacenamientoProvider Provider');
  }

  cargar_storage(){

    let promesa = new Promise(  ( resolve, reject )=>{

      if(  this.platform.is("cordova")   ){
        // Dispositivo
        console.log("Inicializando storage");

        this.storage.ready()
            .then( ()=>{

              console.log("Storage listo");

              this.storage.get("almacen")
                  .then( almacen=>{

                    if( almacen ){
                      this.almacen = almacen;
                    }
                    resolve();
                  });
        })
    }else{
        // Escritorio
        if( localStorage.getItem("almacen")  ){
          this.almacen = JSON.parse( localStorage.getItem("almacen"));
        }
        resolve();
      }
    });
    return promesa;
  }

  guardar_storage(){


    if(  this.platform.is("cordova")   ){
      // Dispositivo
      this.storage.ready()
            .then( ()=>{

              this.storage.set( "almacen", this.almacen );

            })


    }else{
      // Escritorio
      localStorage.setItem("almacen", JSON.stringify(this.almacen) );
    }



  }
}
