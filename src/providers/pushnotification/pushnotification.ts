import { Injectable } from '@angular/core';
import { OneSignal } from '@ionic-native/onesignal';
import { Platform } from 'ionic-angular';


@Injectable()
export class PushnotificationProvider {

  constructor(private oneSignal: OneSignal, public platform:Platform) {
    console.log('Hello PushnotificationProvider Provider');
  }

  init_notifications(){

    if( this.platform.is('cordova') ){

      this.oneSignal.startInit('777fc555-2d7b-4c9d-8dcf-e4fe5ea5e728', '936494068071');
      this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

      this.oneSignal.handleNotificationReceived().subscribe(() => {
       // do something when notification is received
       console.log('Notificacion recibida');
      });

      this.oneSignal.handleNotificationOpened().subscribe(() => {
        // do something when a notification is opened
        console.log('Notificacion abierta');
      });

      this.oneSignal.endInit();

    }else{
      console.log('OneSignal no funciona en Chrome');
    }

  }
}

